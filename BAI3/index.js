/**
 * 
 * Đầu vào:
 * Nhập tiền cần đổi
 * Nhấn đổi tiền để xuất ra số tiền được quy đổi
 * 
 * Các bước xử lí:
 * Gán giá trị cho giá USD hiện nay USD_VND = 23500
 * Tạo biến tien_USD nhận giá trị đầu vào
 * Tạo biến tien_VND = Number(tien_USD) * USD_VND 
 * Xuất giá tiền ra ô kết quả
 * 
 * Đầu ra:
 * Số tiền đổi được ra VND
 * 
 * 
 */
const USD_VND = 23500; 
function doiTien() {
    
    var tien_USD = document.getElementById("money_change").value;
    var tien_VND = Number(tien_USD) * USD_VND;
    document.getElementById("change_success").value = tien_VND;


}