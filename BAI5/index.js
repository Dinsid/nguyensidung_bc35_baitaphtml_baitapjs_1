/**
 * 
 * Đầu vào:
 * Nhập vào một số có 2 chữ số
 * 
 * 
 * Các bước xữ lý:
 * Tạo biến và gán giá trị cho số từ ô nhập giá trị
 * Tạo biến chục và chuc = Math.floor(so / 10)
 * Tạo biến đơn vị và donVi = so%10
 * Tạo biến tính tổng tong = Number(chuc) + Number(donVi)
 * Xuất kết quả ra màn hình
 * 
 * 
 * Đầu ra:
 * tổng 2 ký số của số vừa nhập
 * 
 */

function tinhTong() {
    var so = document.getElementById("number").value;
    var chuc = Math.floor(so / 10);
    var donVi = so % 10;
    var tong = Number(chuc) + Number(donVi);
    document.getElementById("result").innerHTML = tong
}