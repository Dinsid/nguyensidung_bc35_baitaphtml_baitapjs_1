/**
 * 
 * Đầu vào:
 * Chiều dài , chiều rộng của hình chữ nhật
 * 
 * Các bước xữ lý:
 * 
 * 
 * 
 * Đầu ra:
 * Diện tích vè chu vi của hình chữu nhật
 * 
 */


function tinh() {
    var chieuDai = document.getElementById("chieuDai").value;
    var chieuRong = document.getElementById("chieuRong").value;
    var dienTichHCN = Number(chieuDai) * Number(chieuRong);
    var chuViHCN = (Number(chieuDai) + Number(chieuRong))*2;
    var contentResult = `<p> Diện tích: ${dienTichHCN}</p>
                         <p> Chu vi: ${chuViHCN} </p>   `;
    document.getElementById("result").innerHTML = contentResult;
}