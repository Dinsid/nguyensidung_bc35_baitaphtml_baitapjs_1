/**
 * 
 * Đầu vào: 
 * Nhập vào 5 số thực
 * 
 * Các bước xử lý
 * Gán giá trị cho các biến number1, number 2, number3 number4, number5
 * Tính trung bình cộng của 5 số average = (Number(number1) + Number(number2) + Number(number3) + Number(number4) + Number(number5)) / 5
 * Xuất ra ô kết quả
 * 
 * Đầu ra:
 *   Giá trị trung bình của 5 số thực
 * 
 * 
 */



function sumAverage() {
    var number1 = document.getElementById("number1").value;
    var number2 = document.getElementById("number2").value;
    var number3 = document.getElementById("number3").value;
    var number4 = document.getElementById("number4").value;
    var number5 = document.getElementById("number5").value;
    var average = (Number(number1) + Number(number2) + Number(number3) + Number(number4) + Number(number5)) / 5;
    document.getElementById("KetQua").value = average;

}